const request = require('request');

module.exports.setup = function ( pool, user , done, failed) {
    if (pool == null) {
        console.log("Database not connected. Failed creating user.");
        failed();
    } else {
        request({
            url : "https://api-trade.opskins.com/ITrade/GetTradeURL/v1/",
            headers : {
                "Authorization" : "Bearer " + user.access_token,
            }
        },
        function (error, response, result) {
            var body = JSON.parse(result);
            var tradelink = null;
            if (!error && body.status == 1) {
                tradelink = body.response.short_url;
            }
            pool.query("SELECT * FROM users WHERE userid = "+pool.escape(user.id)+";", function (err, result, fields) {
                if (err) {
                    failed();
                    return;
                }
                if (result.length == 0) {
                    pool.query("INSERT IGNORE INTO `users` (`userid`, `trade_link`, `steamid`, `auth_token`) VALUES \
                    ("+pool.escape(user.id)+", "+pool.escape(tradelink)+", "+pool.escape(user.id64)+", "+pool.escape(user.access_token)+");", 
                    function (err,result,fields) {
                        pool.query("INSERT IGNORE INTO `stats` (`userid`) VALUES ("+pool.escape(user.id)+");", function (err,result,fields) {});
                        done();
                    });
                } else {
                    pool.query("UPDATE `users` SET `trade_link`="+pool.escape(tradelink)+", `steamid`="+pool.escape(user.id64)+", `auth_token`="+pool.escape(user.access_token)+" WHERE userid = "+pool.escape(user.id)+";", 
                    function (err,result,fields) {
                        done();
                    });
                }
            });
        }
    );
    }
}; 