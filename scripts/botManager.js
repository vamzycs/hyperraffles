var ExpressTrade = require('expresstrade')
var trade;

module.exports.start = function ( accountData, offerReceived, offerAccepted ) {
    trade = new ExpressTrade({
        apikey: accountData.apiKey,
        twofactorsecret: accountData.twofactorsecret,
        pollInterval: 5000,
    });
    trade.on('offerReceived', (_offer) => {
        offerReceived(_offer);
    });
    trade.on('offerAccepted', (_offer) => {
        offerAccepted(_offer);
    });
}
module.exports.getTradeManager = function () {
    return trade;
}