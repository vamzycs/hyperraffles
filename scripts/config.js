module.exports.db = {
    host: 'localhost',
    user: 'root',
    password: 'fc78c2ad3384bb13f0e2386e7b',
    database: 'raffles',
};

module.exports.opskins = {
    oauth: {
        name: 'HyperRaffles',
        returnURL: 'https://b.rainyskins.com/opskins/callback',
        apiKey: '752df6bc1724f398d8e5b88957e6bf',
        scopes: 'identity_basic',
        mobile: true,
    },
    bots: {
        storage: {
            apiKey: 'a138460fc9e2efc408466d3557c7c2',
            tradeurl: 'https://trade.opskins.com/t/5788963/wRJA8VxU',
            twofactorsecret: 'DZCUXAJIJFRNSOTJ',
        },
        raffles: {
            apiKey: '65fca22e50510e9a66a6b45659e41d',
            tradeurl: 'https://trade.opskins.com/t/5789008/smgo8lLA',
            twofactorsecret: 'BENVZIZJEFUIU2RK',
        },
    },
};

module.exports.ports = {
    web_server: 80,
    socket: 2096,
};