'use strict';
const express = require('express');
const session = require('express-session');
const handlebars = require('express-handlebars');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const md5 = require('md5');
const config = require('./scripts/config');
const userSetup = require('./scripts/userSetup');

var depositBot = require('./scripts/botManager');
depositBot.start( config.opskins.bots.storage, function (_offer) {
    // OfferReceived
    
}, function (_offer) {
    // Offeraccepted
}, );
var rafflesBot = require('./scripts/botManager');
rafflesBot.start( config.opskins.bots.raffles, function (_offer) {
    // OfferReceived
}, function (_offer) {
    // Offeraccepted
}, );

const app = express();

app.set('view engine', 'ejs');

var mysql = require('mysql');

var db_config = config.db;

var pool;
connectDatabase();

const passport = require('passport');
const CustomStrategy = require('passport-custom');
const opAuth = require('opskins-oauth');

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});

let OpskinsAuth = new opAuth.init(config.opskins.oauth);

passport.use('custom', new CustomStrategy(function (req, done) {
    OpskinsAuth.authenticate(req, (err, user) => {
        if (err) {
            done(null);
        } else {
            userSetup.setup(pool, user, function () {
                done(null, user);
            }, function () {
                done(null);
            });
        }
    })
}));

let sessionMiddleware = session({
    key: 'session_id',
    secret: 'hyper',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 1000*60*60*24*365,
    },
});

app.use(cookieParser());
app.use(sessionMiddleware);

app.use(passport.initialize());
app.use(passport.session());

function onAuthorizeSuccess(data, accept) {
    accept();
}

function onAuthorizefail(data, message, error, accept) {
    accept(null, !error);
}

app.get('/login', function (req, res) {
    res.redirect(OpskinsAuth.getFetchUrl());
});

app.get('/opskins/callback', passport.authenticate('custom', {
    failureRedirect: '/',
}), function (req, res) {
    res.redirect('/');
});

app.get('/', function (req, res) {
    res.render('raffles', {
        user: getUserData(req, res),
    });
});

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

app.use(express.static('public'));

app.listen(config.ports.web_server, () => console.log(`HyperRaffles started @:${config.ports.web_server}`));

var socket = require('./scripts/socket');
socket.start();

function connectDatabase() {
    pool = mysql.createConnection(db_config);
    pool.connect(function(err) {
        if(err) {
            console.log('[ERROR] Connecting to database "' + err.toString() + '"');
            setTimeout(function() { connectDatabase(); }, 2500);
        }
        else
        {
          console.log("Database connection established.");
        }
    });
    pool.on('error', function(err) {
        console.log('[ERROR] Syntax "' + err.toString() + '"');
        console.log(err);
        if(err.code == 'PROTOCOL_CONNECTION_LOST') {
            setTimeout(function() { connectDatabase(); }, 2500);
            console.log('[INFO] Trying to reconnect to database...');
        }
        else
        {
            console.log('[ERROR] Connecting to database ' + err.toString());
        }
    });
    setInterval(database_heartbeat, 5000);
}

function getUserData (req, res) {
    if (req.session != null && req.session.passport != null && req.session.passport.user != null) {
        return req.session.passport.user;
    } else {
        return null;
    }
}

function database_heartbeat() {
  if (pool != null) {
    pool.query("SELECT 1", function (err,result,fields) {});
  }
}

/*
Bot Trading
*/



var depositBot = require('./scripts/botManager');
depositBot.start( config.opskins.bots.storage, function (_offer) {
    // OfferReceived

}, function (_offer) {
    // Offeraccepted

}, );

var rafflesBot = require('./scripts/botManager');
rafflesBot.start( config.opskins.bots.raffles, function (_offer) {
    // OfferReceived

}, function (_offer) {
    // OfferAccepted

}, );

/* Raffle data */

function createRaffle () {

}